**Task 5 - Java 8 API**

There are 2 log files **start.log** and **end.log** that contain start and end data of the best laps for  
each racer of Formula 1 - Monaco 2018 Racing. Data contains only the first 20 minutes that  
refers to the first stage of the qualification.

The third file **abbreviations.txt** contains abbreviation explanations.

Your task is to read data from 2 files, order racers by time and print report that shows the top 15  
racers and the rest after underline. (See examples in the docs folder of the project).

Use Java 8 API where appropriate.